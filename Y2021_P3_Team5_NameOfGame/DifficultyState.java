import greenfoot.*;

public class DifficultyState extends State {

    public DifficultyState(GameWorld w) {
        super(w);
    }

    public void onSet() {
       getWorld().setBackground("DifficultyTitle.png");
       Button a = new Button();
       getWorld().addObject(a, 300, 390);
       Button b = new Button();
       getWorld().addObject(b, 300, 590);
       Button c = new Button();
       getWorld().addObject(c, 300, 790);
    }
    
    public void onClick() {
        Actor h = getWorld().getObjectsAt(300, 390, Button.class).get(0);
        Actor m = getWorld().getObjectsAt(300, 590, Button.class).get(0);
        Actor e = getWorld().getObjectsAt(300, 790, Button.class).get(0);
        
        if (h != null && m != null && e != null) {
            if (Greenfoot.mouseClicked(h) == true) {
                int pipeGap = 270;
                int pipeSpawn = 150;
                int coinSpawn = 350;
                int nukeSpawn = 4000;
                GameState mstate = new GameState(getWorld(), pipeGap, pipeSpawn, coinSpawn, nukeSpawn);
                getWorld().setState(mstate);
                MyWorld myworld = (MyWorld)getWorld();
                myworld.setScore(0);
            } else if (Greenfoot.mouseClicked(m) == true) {
                int pipeGap = 300;
                int pipeSpawn = 200;
                int coinSpawn = 500;
                int nukeSpawn = 4000;
                GameState mstate = new GameState(getWorld(), pipeGap, pipeSpawn, coinSpawn, nukeSpawn);
                getWorld().setState(mstate);
                MyWorld myworld = (MyWorld)getWorld();
                myworld.setScore(0);
            } else if (Greenfoot.mouseClicked(e) == true) {
                int pipeGap = 330;
                int pipeSpawn = 250;
                int coinSpawn = 650;
                int nukeSpawn = 4000;
                GameState mstate = new GameState(getWorld(), pipeGap, pipeSpawn, coinSpawn, nukeSpawn);
                getWorld().setState(mstate);
                MyWorld myworld = (MyWorld)getWorld();
                myworld.setScore(0);
            }
        }
    }
    
    public void onAct() {
        onClick();
    }
}