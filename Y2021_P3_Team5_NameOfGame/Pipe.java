import greenfoot.*; 

public class Pipe extends Scrollers {
    int x;
    int y;

    public Pipe(int dir) {
        if (dir == 0) {
            setImage("PipeUp.png");
        } else if (dir == 1) {
            setImage("PipeDown.png");
        }
    }

    public void act() {
        super.act();
        x = this.getX();
        y = this.getY();

        checkLocation(x, y);
        
    }
}