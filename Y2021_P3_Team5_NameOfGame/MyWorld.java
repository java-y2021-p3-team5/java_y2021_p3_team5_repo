import greenfoot.*; 

public class MyWorld extends GameWorld {

    int score;
    int highscore;
    Text txt;
    int frames = 0;
    int countBack = 0;
    
    public MyWorld() {
        super(575, 863, 1, false); 

        TitleState tstate = new TitleState(this);
        setState(tstate);

        score = 0;
        highscore = 0;
        txt = new Text("0", 60, Color.WHITE);

    }
    
    public void act(){
        super.act();
        
        if(countBack == 0){
            GreenfootSound sound = new GreenfootSound("BackgroundMusic.mp3"); // create a sound object
            sound.setVolume(45); //Sounds in decibels.     
            sound.play();
            countBack++;
        }
        
        frames++;
        if(frames % (60 * 216) == 0){
            GreenfootSound sound = new GreenfootSound("BackgroundMusic.mp3"); // create a sound object
            sound.setVolume(45); //Sounds in decibels.     
            sound.play();
            frames = 0;
        }
    }

    public void setScore(int s) {
        score = s;
        String st = s + "";
        txt.setText(st);
    }

    public void setHighScore(int s) {
        highscore = s;
    }

    public int getScore() {
        return score;

    }

    public int getHighScore() {
        return highscore;

    }

    public Text getText() {
        return txt;

    }
}