import greenfoot.*;

public class Scrollers extends Actor {

    private int speed = -3;
    
    public void setSpeed(int s) {
        speed = s;
    }
    
    public void act() {
        move(speed);

    }  

    public void checkLocation(int x, int y){
        World world = getWorld();

        if(x + getImage().getWidth() < 0){
            world.removeObject(this);
        }
    }

}
