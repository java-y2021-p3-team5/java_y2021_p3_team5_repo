public abstract class State {
    
    private GameWorld world;

    public State(GameWorld w) {
        world = w;
    }

    public GameWorld getWorld() {
        return world;
    }
    
    public abstract void onSet();
    
    public void onRemove() {
        java.util.List objects = getWorld().getObjects(null);
        getWorld().removeObjects(objects);
    }
    
    public void onAct() {
        
    }
}