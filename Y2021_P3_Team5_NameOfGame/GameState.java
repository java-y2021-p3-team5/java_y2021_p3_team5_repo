import greenfoot.*;

public class GameState extends State {

    int counter = 0;
    int pipeGap;
    int pipeSpawn;
    int coinSpawn;
    int nukeSpawn;

    public GameState(GameWorld w, int g, int p, int c, int n) {
        super(w);
        pipeGap = g;
        pipeSpawn = p;
        coinSpawn = c;
        nukeSpawn = n;
    }

    public void onSet() {
        Bird flappy = new Bird();
        getWorld().addObject(flappy, getWorld().getWidth() / 2, getWorld().getHeight() / 2);

        int ran = Greenfoot.getRandomNumber(4) + 1;
        getWorld().setBackground("Background" + ran + ".png");
        
        MyWorld myworld = (MyWorld)getWorld();
        getWorld().addObject(myworld.getText(), getWorld().getWidth() / 2, getWorld().getHeight() / 5);
    }

    public void onAct() {
        counter++;

        if (counter % pipeSpawn == 0) {
            Pipe pipe1 = new Pipe(0);
            int y = Greenfoot.getRandomNumber(pipe1.getImage().getHeight() - 100) + getWorld().getHeight() - pipe1.getImage().getHeight();
            getWorld().addObject(pipe1, getWorld().getWidth() + pipe1.getImage().getWidth() / 2, y + pipe1.getImage().getHeight() / 2);

            Pipe pipe2 = new Pipe(1);
            getWorld().addObject(pipe2, getWorld().getWidth() + pipe2.getImage().getWidth() / 2, y - pipeGap - pipe2.getImage().getHeight() / 2);
        }

        int addcoin = Greenfoot.getRandomNumber(coinSpawn);
        if(addcoin == 1) {
            Coin coin = new Coin();
            int y = Greenfoot.getRandomNumber(getWorld().getHeight() - coin.getImage().getHeight()) + coin.getImage().getHeight() / 2;
            getWorld().addObject(coin, getWorld().getWidth() + coin.getImage().getWidth() / 2, y);
        }

        int addnuke = Greenfoot.getRandomNumber(nukeSpawn);
        if(addnuke == 1) {
            Nuke nuke = new Nuke();
            int y = Greenfoot.getRandomNumber(getWorld().getHeight() - nuke.getImage().getHeight()) + nuke.getImage().getHeight() / 2;
            getWorld().addObject(nuke, getWorld().getWidth() + nuke.getImage().getWidth() / 2, y);
        }

        getWorld().setPaintOrder(Bird.class, Pipe.class, Nuke.class, Coin.class);
    }
}