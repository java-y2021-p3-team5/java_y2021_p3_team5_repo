import greenfoot.*;

public class TitleState extends State {

    public TitleState(GameWorld w) {
        super(w);
    }

    public void onSet() {
       getWorld().setBackground("TitleScreen.png");
       Button a = new Button();
       getWorld().addObject(a, 100, 575);
       Button b = new Button();
       getWorld().addObject(b, 290, 380);
       Button c = new Button();
       getWorld().addObject(c, 475, 575);
    }
    
    public void onClick() {
        Actor s = getWorld().getObjectsAt(100, 575, Button.class).get(0);
        Actor i = getWorld().getObjectsAt(475, 575, Button.class).get(0);
        Actor d = getWorld().getObjectsAt(290, 380, Button.class).get(0);
        
        if (s != null && i != null && d != null) {
            if (Greenfoot.mouseClicked(s) == true) {
                ScoreState sstate = new ScoreState(getWorld());
                getWorld().setState(sstate);
            } else if (Greenfoot.mouseClicked(i) == true) {
                InfoState istate = new InfoState(getWorld());
                getWorld().setState(istate);
            } else if (Greenfoot.mouseClicked(d) == true) {
                DifficultyState dstate = new DifficultyState(getWorld());
                getWorld().setState(dstate);
            }
        }
    }
    
    public void onAct() {
        onClick();
    }
}