import greenfoot.*;
 
public class ScoreState extends State {
    
    
    
    public ScoreState(GameWorld w) {
        super(w);
    }

    public void onSet() {
       getWorld().setBackground("HighScoresBackground.png");
       Button back = new Button();
       getWorld().addObject(back, 270, 590);
       MyWorld myworld = (MyWorld)getWorld();
       String s = myworld.getScore() + "";
       Text t = new Text(s, 40, Color.BLACK);
       getWorld().addObject(t, 330, 392);
       if (myworld.getScore() > myworld.getHighScore()) {
            myworld.setHighScore(myworld.getScore());
            Text n = new Text("New High Score!", 40, Color.GREEN);
            getWorld().addObject(n, 278, 350);
       }
       String l = myworld.getHighScore() + "";
       Text h = new Text(l, 40, Color.BLACK);
       getWorld().addObject(h, 330, 479);
    }

    public void onClick() {
        Actor b = getWorld().getObjectsAt(270, 590, Button.class).get(0);

        if (b != null) {
            if (Greenfoot.mouseClicked(b) == true) {
                TitleState tstate = new TitleState(getWorld());
                getWorld().setState(tstate);
            }
        }
    }

    public void onAct() {
        onClick();
    }
}