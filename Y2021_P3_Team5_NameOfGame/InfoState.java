import greenfoot.*;

public class InfoState extends State {

    public InfoState(GameWorld w) {
        super(w);
    }

    public void onSet() {
       getWorld().setBackground("InfoBackground.png");
       Button back = new Button();
       getWorld().addObject(back, 123, 720);
    }
    
    public void onClick() {
        Actor b = getWorld().getObjectsAt(123, 720, Button.class).get(0);
        
        if (b != null) {
            if (Greenfoot.mouseClicked(b) == true) {
                TitleState tstate = new TitleState(getWorld());
                getWorld().setState(tstate);
            }
        }
    }
    
    public void onAct() {
        onClick();
    }
}