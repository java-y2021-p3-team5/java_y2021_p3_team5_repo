import java.awt.Rectangle;
import greenfoot.*;

public class Bird extends Actor {

    private double gravity = -10;
    private int counter = 0;
    private boolean jump = true;
    private double fY = 0.31; // Changes the gravity Speed
    private int jumpSpeedCount = 0; // Decides how long they will go upwards in a jump;
    private int gameOverCount = 0;
    private int hitSoundCount = 0;
    private boolean stopscroll = false;
    private int scrollcount = 0;
    private int num = 0;
    boolean gameover = false;
    int scoreCount = 0;
    boolean hitpipe = false;
    
    public void increaseScore() {
        MyWorld myworld = (MyWorld)getWorld();
        myworld.setScore(myworld.getScore() + 10);
    }
    
    public void gameOver() {
        if(gameOverCount == 80){
            ScoreState s = new ScoreState((GameWorld)getWorld());
            ((GameWorld)getWorld()).setState(s);
        }
        gameOverCount++;
        GreenfootSound sound = new GreenfootSound("Death.wav"); // create a sound object
        sound.setVolume(80); //Sounds in decibels. 
        if(hitSoundCount == 0){
            sound.play(); 
            hitSoundCount++;
        }
    }

    public void act() {
        //increasing score
        if (jump == true) {
            scoreCount++;
        }
        
        if(scoreCount % 200 == 0) {
            increaseScore();
        }
        
        //Changes the Image
        counter++;
        if (counter % 30 == 0 && jump) {
            setImage("Bird1.png");
        } else if (counter % 15 == 0 && jump) {
            setImage("Bird2.png");
        }
        
        //Jumping and Gravity 
        if (Greenfoot.getKey() == "space" && jump) {
            jumpSpeedCount++;
            GreenfootSound sound = new GreenfootSound("BirdFlap.wav"); // create a sound object
            sound.setVolume(60); //Sounds in decibels. 
            sound.play(); // play the sound
            gravity = -10;
            fY = 2;
            setLocation(getX(), getY() + (int)gravity);
            setRotation(359);
        } else if (jumpSpeedCount >= 1 && jumpSpeedCount <= 3) {
            setRotation(getRotation() - 10);
            fY -= 1.2;
            gravity -= fY;
            setLocation(getX(), getY() + (int)gravity);
            jumpSpeedCount++;
        }else {
            fY = 0.31;
            gravity = gravity + fY;
            setLocation(getX(), getY() + (int)gravity);
            
            if (gameover == false) {
                if (getRotation() >= 270) {
                    setRotation(getRotation() + 1);
                } else if (getRotation() >= 367) { 
                    setRotation(0);
                } else if((getRotation() + 1 <= 90 && getRotation() >= 0)) {
                    setRotation(getRotation() + 1);
                }
            }
            
            jumpSpeedCount = 0;
        }
        
        //Location boundaries for actor
        int x = this.getX();
        int y = this.getY();
        if(y - getImage().getHeight() / 2 < 0){
            setLocation(x, 0 + getImage().getHeight() / 2);
        } else if (y + getImage().getHeight() / 2 > getWorld().getHeight() || jump == false){
            
            for (int i = 0; i < getWorld().getObjects(Scrollers.class).size(); i++) {
                getWorld().getObjects(Scrollers.class).get(i).setSpeed(0);
            }
            
            if(y + getImage().getHeight() / 2 > getWorld().getHeight()){
                setLocation(x,getWorld().getHeight() - getImage().getHeight() / 2);
                jump = false;
                setRotation(90);
                gameover = true;
            }
        }
        
        //Finds out if the actor is interacting with another actor
        Actor intercoin = getOneIntersectingObject(Coin.class);
        if (intercoin != null && jump) {
            GreenfootSound sound = new GreenfootSound("CoinPickup.wav"); // create a sound object
            sound.setVolume(80); //Sounds in decibels. 
            sound.play(); // play the sound
            getWorld().removeObject(intercoin);
            increaseScore();
            increaseScore();
            increaseScore();
        }
        
        Actor internuke = getOneIntersectingObject(Nuke.class);
        if (internuke != null && jump) {
            getWorld().removeObject(internuke);
            GreenfootSound sound = new GreenfootSound("Nuke.aiff"); // create a sound object
            sound.setVolume(100); //Sounds in decibels. 
            sound.play(); // play the sound
            stopscroll = true;
        }
        
        /*
        // PIXEL PERFECT STARTS HERE
        Actor interpipe = getOneIntersectingObject(Pipe.class);
        if (interpipe != null) {
            // Corresponding rectangles for each image
            Rectangle rect1 = new Rectangle(getX() - getImage().getWidth() / 2, getY() - getImage().getHeight() / 2, getImage().getWidth(), getImage().getHeight());
            Rectangle rect2 = new Rectangle(interpipe.getX() - interpipe.getImage().getWidth() / 2, interpipe.getY() - interpipe.getImage().getHeight() / 2, interpipe.getImage().getWidth(), interpipe.getImage().getHeight());
            Rectangle intersect = rect1.intersection(rect2);
            
            // confusing stuff; the whole conversion with world coodinates to img coordinates
            int startx1 = getImage().getWidth() / 2 + (int) intersect.getX() - getX();
            int starty1 = getImage().getHeight() / 2 + (int) intersect.getY() - getY();
            int startx2 = interpipe.getImage().getWidth() / 2 + (int) intersect.getX() - interpipe.getY();
            int starty2 = interpipe.getImage().getHeight() / 2 + (int) intersect.getY() - interpipe.getY();
            boolean stop1 = false;
            boolean stop2 = false;
            
            // looping through every pixel in first image intersection area
            for (int x1 = startx1; x1 < intersect.getWidth(); x1++) {
                for (int y1 = starty1; y1 < intersect.getHeight(); y1++) {
                    if (getImage().getColorAt(x1, y1).getAlpha() != 0) {
                        stop1 = true;
                    }
                }
            }
            
            // looping through every pixel in second image intersection area
            for (int x2 = startx2; x2 < intersect.getWidth(); x2++) {
                for (int y2 = starty2; y2 < intersect.getHeight(); y2++) {
                    if (interpipe.getImage().getColorAt(x2, y2).getAlpha() != 0) {
                        stop2 = true;
                    }
                }
            }
            
            // checks if there are non transparent pixels in both of the images intersecting parts
            if (stop1 && stop2) {
                hitpipe = true;
            }
        }
        
        //what to do if there is actually a collision
        if (hitpipe == true) {
            jump = false;
            
            for (int i = 0; i < getWorld().getObjects(Scrollers.class).size(); i++) {
                getWorld().getObjects(Scrollers.class).get(i).setSpeed(0);
            }
        }
        // ENDS HERE
        */
        
        Actor interpipe = getOneIntersectingObject(Pipe.class);
        if (interpipe != null) {
            jump = false;
            
            for (int i = 0; i < getWorld().getObjects(Scrollers.class).size(); i++) {
                getWorld().getObjects(Scrollers.class).get(i).setSpeed(0);
            }
        }
               
        //Code for nuke background
        if (stopscroll == true && gameover == false) {
            scrollcount++;            
            getWorld().removeObjects(getWorld().getObjects(Pipe.class));
            
            if (num < 30 && scrollcount % 15 == 0) {
                num++;
                getWorld().setBackground("NukeBackground" + num + ".png");
            }
            
            if(num == 30 && scrollcount == 451) {
                int ran = Greenfoot.getRandomNumber(4) + 1;
                getWorld().setBackground("Background" + ran + ".png");
            }
            
            if (scrollcount >= 500) {
                stopscroll = false;
                scrollcount = 0;
                num = 0;
            }
        }
        
        //What to do it game is over
        if (gameover == true) {
            gameOver();
        }
    }
}