import greenfoot.*;

public abstract class GameWorld extends World {
    
    private State state = null;
    
    public GameWorld(int width, int height, int cellSize) {    
        super(width, height, cellSize); 
    }
    
    public GameWorld(int width, int height, int cellSize, boolean bounded) {    
        super(width, height, cellSize, bounded); 
    }
    
    public void setState(State s) {
        if(state != null) {
            state.onRemove();
        }
        
        state = s;
        state.onSet();
    }
    
    public void act() {
        if(state != null) {
            state.onAct();
        }
    }
}