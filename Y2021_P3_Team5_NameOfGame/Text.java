import greenfoot.*;

public class Text extends Actor {
    
    private String text;
    private int size;
    private Color foreground;
    private Color background;
    private Color outline;
    
    public Text() {
        text = "Text";
        size = 25;
        foreground = Color.BLACK;
        background = Color.WHITE;
        outline = Color.RED;
        
        updateImage();
    }
    
    public Text(String t) {
        text = t;
        size = 20;
        foreground = Color.WHITE;
        background = null;
        outline = null;
        
        updateImage();
    }
    
    public Text(String t, int s, Color f) {
        text = t;
        size = s;
        foreground = f;
        background = null;
        outline = null;
        
        updateImage();
    }
    
    public Text(String t, int s, Color f, Color b, Color o) {
        text = t;
        size = s;
        foreground = f;
        background = b;
        outline = o;
        
        updateImage();
    }
    
    public String getText() {
        return text;
    }
    
    public int getSize() {
        return size;
    }
    
    public Color getForeground() {
        return foreground;
    }
    
    public Color getBackground() {
        return background;
    }
    
    public Color getOutline() {
        return outline;
    }
    
    public void setText(String t, int s, Color f, Color b, Color o) {
        text = t;
        size = s;
        foreground = f;
        background = b;
        outline = o;
        
        updateImage();
    }
    
    public void setText(String t) {
        text = t;
        updateImage();
    }
    
    public void setSize(int s) {
        size = s;
        updateImage();
    }
    
    public void setForeground(Color f) {
        foreground = f;
        updateImage();
    }
    
    public void setBackground(Color b) {
        background = b;
        updateImage();
    }
    
    public void setOutline(Color o) {
        outline = o;
        updateImage();
    }
    
    public void updateImage() {
        GreenfootImage img = new GreenfootImage(text, size, foreground, background, outline);
        setImage(img);
    }
    
    public void act() {
        // Add your action code here.
    }    
}