# Floppy Bird Project #

## Group Information ##

**Team Members**
Ethan Layne
Harshit Aswale
Mira Panigrahy

**Group Number:** 5

**Period:**	3

**Game Title:** Floppy Bird

## Game Proposal ##
We will make a copy of the game Flappy Bird called Floppy Bird. We will be adding collectable coins and different background images that change randomly every time the game is reset to make the game more interesting. We will also be adding powerups that spawn in once in a while and remove the pipes for a few seconds. We will be using different sprite images that are hand drawn.

Game Controls:

Space key controls how high the player goes up. And if released, the bird goes down.
Enter key starts the game.

Game Elements:

+ Simple falling physics in a gravity world
+ Non-moving, and moving objects
+ An infinite scroller
+ Personal High-Score
+ As score increases, difficulty level increases until reaches cap
+ If bird hits an object it dies and game is over
+ Collectable coins that increase score
+ Powerups/Nukes that stop the pipes from spawning for a few seconds

How to Win:

+ There is no winning, but rather how high of a score you can achieve. 
+ The game will not end by successfully stopping the player from hitting a pipe 
+ You will recive points that increase your score by collecting as many coins as possible.

## Link Examples ##
https://www.youtube.com/watch?v=fQoJZuBwrkU

YouTube videos of gameplay, etc.

+ [Example Link](http://www.freewebarcade.com/game/tiny-empire/)

## Teacher Response ##

Your teacher can add comments and suggestions here

## Class Design and Brainstorm ##

Put all your brainstorm ideas, strategy approaches, and class outlines here

#Classes:# 
Player/Bird
Pipes
Scrolling
Text
Coin
State
Powerups class (Possibility) 